<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim"method="post">
        @csrf
        <label>First Name</label> <br><br>
        <input type="text" name="firstname"> <br><br>
        <label>Last Name</label> <br><br>
        <input type="text" name="lastname"> <br><br>
        <label>Jenis Kelamin</label> <br> <br>
        <input type="radio" name="jk" value="1">Male <br>
        <input type="radio" name="jk" value="2">Female <br> <br>
        <label for="">Nationality </label><br><br>
        <input type="checkbox" id="Indonesia"><label for="Indonesia">Indonesia</label><br>
        <input type="checkbox" id="Palestina"><label for="Palestina">Palestina</label><br>
        <input type="checkbox" id="Italia"><label for="Italia">Italia</label> <br><br>
        <label for="">Language Spoken </label><br><br>
        <input type="checkbox" id="Bahasa Indonesia"><label for="Bahasa Indonesia">Bahasa Indonesia </label><br>
        <input type="checkbox" id="English"><label for="English">English </label><br>
        <input type="checkbox" id="Other"><label for="Other">Other</label> <br><br>
        <label>Biodata</label> <br> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br> <br>
        <input type="submit" value="sign up">
    </form>
</body>
</html>