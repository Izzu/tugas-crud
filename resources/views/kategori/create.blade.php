@extends('layout.master')

@section('judul')
Tambah Kategori
@endsection

@section('content')

        <form action="/kategori" method="POST">
            @csrf
            <div class="form-group">
                <label>nama</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Title">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>deskripsi</label>
                <textarea name="deskripsi" class="form-control" cols="30" rows="10"></textarea>
                @error('deskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>

@endsection